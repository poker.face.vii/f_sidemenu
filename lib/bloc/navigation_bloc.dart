import 'package:bloc/bloc.dart';
import 'package:side_menu/pages/accountPage.dart';

import 'package:side_menu/pages/homePage.dart';
import 'package:side_menu/pages/orderPage.dart';

enum NavigationEvents { HomeClickEvent, OrderClickEvent, AccuntClickEvent }

abstract class NavigatorStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigatorStates> {
  @override
  NavigatorStates get initialState => HomePage();

  @override
  Stream<NavigatorStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomeClickEvent:
        yield HomePage();
        break;
      case NavigationEvents.OrderClickEvent:
        yield OrderPage();
        break;
      case NavigationEvents.AccuntClickEvent:
        yield AccountPage();
        break;
    }
    
  }
}
