import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:side_menu/bloc/navigation_bloc.dart';
import 'package:side_menu/sidebar/menu_item.dart';

class Sidebar extends StatefulWidget {
  @override
  _SidebarState createState() => _SidebarState();
}

class _SidebarState extends State<Sidebar>
    with SingleTickerProviderStateMixin<Sidebar> {
  StreamController<bool> isSidebarOpenStreamController;
  Stream<bool> isSidebarOpenedStream;
  StreamSink<bool> isSidebarOpenedSink;

  AnimationController _animationController;

  final _animatedDuration = const Duration(milliseconds: 400);

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: _animatedDuration);
    isSidebarOpenStreamController = PublishSubject<bool>();
    isSidebarOpenedStream = isSidebarOpenStreamController.stream;
    isSidebarOpenedSink = isSidebarOpenStreamController.sink;
  }

  @override
  void dispose() {
    _animationController.dispose();
    isSidebarOpenStreamController.close();
    isSidebarOpenedSink.close();
    super.dispose();
  }

  void onIconPressed() {
    final animationStatus = _animationController.status;
    final isAnimationComplited = animationStatus == AnimationStatus.completed;
    if (isAnimationComplited) {
      isSidebarOpenedSink.add(false);
      _animationController.reverse();
    } else {
      isSidebarOpenedSink.add(true);
      _animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return StreamBuilder<bool>(
      initialData: false,
      stream: isSidebarOpenedStream,
      builder: (context, isSidebarOpenedAsync) {
        return AnimatedPositioned(
          duration: _animatedDuration,
          top: 0,
          bottom: 0,
          left: isSidebarOpenedAsync.data ? 0 : -screenWidth,
          right: isSidebarOpenedAsync.data ? 5 : screenWidth - 43,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  color: Color(0xFF262AAA),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 100,
                      ),
                      ListTile(
                        title: Text(
                          'Ralph',
                          style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        subtitle: Text(
                          "ralph@gmail.com",
                          style: TextStyle(
                            fontSize: 20,
                            color: Color(0xFF1BB5FD),
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                        leading: CircleAvatar(
                          child: Icon(
                            Icons.perm_identity,
                            color: Colors.white,
                          ),
                          radius: 40,
                        ),
                      ),
                      Divider(
                        height: 64,
                        thickness: .5,
                        color: Colors.cyanAccent.withOpacity(.4),
                        indent: 32,
                        endIndent: 32,
                      ),
                      MenuItem(
                        icon: Icons.home,
                        title: "HOME",
                        onTap:(){BlocProvider.of<NavigationBloc>(context).add(NavigationEvents.HomeClickEvent);onIconPressed();},
                      ),
                      MenuItem(
                        icon: Icons.person,
                        title: "My Account",
                        onTap:(){BlocProvider.of<NavigationBloc>(context).add(NavigationEvents.AccuntClickEvent);onIconPressed();},
                      ),
                      MenuItem(
                        icon: Icons.shopping_basket,
                        title: "My Order",
                        onTap:(){BlocProvider.of<NavigationBloc>(context).add(NavigationEvents.OrderClickEvent);onIconPressed();},
                      ),
                      MenuItem(
                        icon: Icons.card_giftcard,
                        title: "Wish List",
                      ),
                      Divider(
                        height: 64,
                        thickness: .5,
                        color: Colors.cyanAccent.withOpacity(.4),
                        indent: 32,
                        endIndent: 32,
                      ),
                      MenuItem(
                        icon: Icons.settings,
                        title: "Settings",
                      ),
                      MenuItem(
                        icon: Icons.exit_to_app,
                        title: "Logout",
                      )
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment(0, -0.9),
                child: GestureDetector(
                  onTap: () {
                    onIconPressed();
                  },
                  child: ClipPath(
                    clipper: CustomMenuClipper(),
                    child: Container(
                      width: 35,
                      height: 110,
                      color: Color(0xFF262AAA),
                      alignment: Alignment.centerLeft,
                      child: AnimatedIcon(
                        progress: _animationController.view,
                        icon: AnimatedIcons.menu_arrow,
                        color: Color(0xFF1BB5FD),
                        size: 30,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

class CustomMenuClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Paint paint = Paint();
    paint.color = Colors.white;
    Path path = Path();
    final width =size.width;
    final height = size.height;

    path.moveTo(0,0);
    path.quadraticBezierTo(0, 8, 10, 16);
    path.quadraticBezierTo(width-1, height/2-20, width, height/2);
    path.quadraticBezierTo(width-1, height/2+20, 10, height-16);
    path.quadraticBezierTo(0, height-8, 0, height);



    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
